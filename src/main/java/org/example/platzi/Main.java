package org.example.platzi;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws SQLException {
        Conexion conexion = new Conexion();
        Scanner scanner = new Scanner(System.in);
        int opcion = 0;
        do{
            System.out.println(" ------------ ");
            System.out.println("Mensajería:");
            System.out.println("1. Crear mensaje");
            System.out.println("2. Listar mensajes");
            System.out.println("3. editar mensaje");
            System.out.println("4. eliminar mensaje");
            System.out.println("5. Salir");
            System.out.println(" ------------ ");
            opcion = scanner.nextInt();
            switch (opcion){
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.actualizarMensaje();
                    break;
                case 4:
                    MensajesService.eliminarMensaje();
                    break;
                default:
                    break;
            }
        }
        while(opcion != 5);
        try{
            Connection cnx = conexion.getConnection();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}