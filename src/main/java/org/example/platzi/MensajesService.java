package org.example.platzi;

import java.sql.SQLException;
import java.util.Scanner;

public class MensajesService {
    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe un mensaje");
        String mensaje = sc.nextLine();
        System.out.println("Ingresa el autor");
        String autor = sc.nextLine();
        Mensajes m = new Mensajes();
        m.setMensaje(mensaje);
        m.setAutor_mensaje(autor);
        MensajesDAO.crearMensaje(m);
    }
    public static void listarMensajes() throws SQLException {
        MensajesDAO.obtenerMensaje();
    }
    public static void eliminarMensaje() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingresa el id del mensaje a eliminar: ");
        int id = sc.nextInt();
        MensajesDAO.eliminarMensaje(id);
    }
    public static void actualizarMensaje() throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el nuevo mensaje : ");
        String mensaje = sc.nextLine();
        System.out.println("Ingresa el id del mensaje a actualizar: ");
        int id = sc.nextInt();
        Mensajes m = new Mensajes();
        m.setId_mensajes(id);
        m.setMensaje(mensaje);
        MensajesDAO.actualizarMensaje(m);
    }

}
