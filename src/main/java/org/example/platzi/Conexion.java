package org.example.platzi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    public Connection getConnection() {
        Connection conection = null;
        try {
            conection = DriverManager.getConnection("jdbc:mysql://localhost:3308/mensajes_app", "root" , "");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conection;
    }
}
