package org.example.platzi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDAO {
    public static void crearMensaje(Mensajes mensajes) {
        // Hago una instancia del objeto que conecta a la BD
        Conexion db_connect = new Conexion();
        try(Connection conect = db_connect.getConnection()){
            PreparedStatement ps = null;
            try{
                String query = "INSERT INTO `mensajes` (`mensaje`, `autor_mensaje`) VALUES (? ,?);";
                ps = conect.prepareStatement(query);
                ps.setString(1, mensajes.getMensaje());
                ps.setString(2, mensajes.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje creado");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void actualizarMensaje(Mensajes mensajes) throws SQLException {
        Conexion db_connect = new Conexion();
        try(Connection conect = db_connect.getConnection()) {
            PreparedStatement ps = null;
            try{
                String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
                ps = conect.prepareStatement(query);
                ps.setString(1, mensajes.getMensaje());
                ps.setInt(2, mensajes.getId_mensajes());
                ps.executeUpdate();
                System.out.println("Mensaje editado");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
    public static void eliminarMensaje(int id_mensaje) {
        Conexion db_connect = new Conexion();

        try(Connection conect = db_connect.getConnection()){
            PreparedStatement ps = null;
            try{
                String query = "DELETE FROM `mensajes` WHERE `id_mensaje` = ?";
                ps = conect.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate();
                System.out.println("Mensaje eliminado");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void obtenerMensaje() throws SQLException {
        Conexion db_connect = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try(Connection conect = db_connect.getConnection()){
            String query = "SELECT * FROM `mensajes`";
            ps = conect.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("id_mensaje: " + rs.getInt("id_mensaje"));
                System.out.println("mensaje: " + rs.getString("mensaje"));
                System.out.println("autor_mensaje: " + rs.getString("autor_mensaje"));
                System.out.println("fecha: " + rs.getString("fecha_mensaje"));
            }

        }

    }
}
